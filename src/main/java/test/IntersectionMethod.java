package test;

import java.awt.image.Raster;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

import org.geotools.filter.text.cql2.CQLException;
import org.geotools.geometry.jts.JTSFactoryFinder;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;

public class IntersectionMethod {
	
	public static void intersect_method(String countryname) throws CQLException, IOException{
		//Change raster file
		Raster raster = Readimage.readimage("glc2000_v1_1.tif");
		GetRasterSize.getrastersize("glc2000_v1_1.tif");
		Vector<Geometry> polygons = Decompose.get_decompose_array(countryname);
		Vector<Coordinate> pionts_inside_geom = new Vector<Coordinate>();
		
		for (int index = 0; index < polygons.size(); index++){
			Envelope env = GetEnvolope.getenvelope(polygons.elementAt(index));
			Vector<Coordinate> pionts_inside_polygon = new Vector<Coordinate>();
			Vector<LineString> boundarys = get_boundary_segments(polygons.elementAt(index));
			
			
			
			double minx_vector = env.getMinX();
			double maxx_vector = env.getMaxX();
			
			int y_up = Rounding.getfloor(RVConversion.vectortoraster_y(env.getMaxY()));
			int y_down = Rounding.getcelling(RVConversion.vectortoraster_y(env.getMinY()));
			
			Vector<Vector<LineString>> s = get_eachline_segments(y_up, y_down, boundarys);
//			for (int j = y_up; j < y_down; j++){
//				System.out.println(s.get(j));
//			}
			
//			System.out.println(y_up +" " + y_down);
			for (int j = y_up; j < y_down; j++){
//				System.out.println("Now is at line " + j);
				
				Vector<Coordinate> inside_points = get_inside_points(j, minx_vector, maxx_vector, s.get(j));

//				System.out.println("Points inside polygon at line " + j + " is " + inside_points.size());
				
//				Vector<Coordinate> inside_points_vector = new Vector<Coordinate>();
//				for (int i = 0; i <  inside_points.size(); i++){
//					Coordinate point_in_vector = new  Coordinate();
//					point_in_vector.x = RVConversion.rastertovector_x(inside_points.elementAt(i).x);
//					point_in_vector.y = RVConversion.rastertovector_y(inside_points.elementAt(i).y);
//					inside_points_vector.add(point_in_vector);
//					System.out.print(point_in_vector);
//				}
				
				
				
				
				pionts_inside_polygon.addAll(inside_points);		
			}
			pionts_inside_geom.addAll(pionts_inside_polygon) ;
		}
		double result = AggregateOfPoints.aggregate(raster, pionts_inside_geom);
		System.out.println("Intersection method number of pixels equals " + pionts_inside_geom.size());
	}
	
	public static Vector<LineString> get_boundary_segments (Geometry polygon){
		Coordinate[] coords = polygon.getCoordinates();
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();

//		MultiLineString boundary =  (MultiLineString) polygon1.getBoundary();
		Vector<LineString> segments = new Vector<LineString>();

		for (int i = 0; i < coords.length - 1; i++){
			Coordinate[] terminal  = new Coordinate[] {coords[i], coords[i+1]};
			LineString line = geometryFactory.createLineString(terminal);
			segments.add(line);
		}
//		System.out.println("The polygon's number of boundary segments is " + segments.size());
		return segments;
	}
	
	public static Vector<Vector<LineString>> get_eachline_segments (int y_up,  int y_down, Vector<LineString> boundarys){
		Vector<Vector<LineString>> s = new Vector<Vector<LineString>>(y_down);
		for (int j = 0; j <= y_down; j++){
			s.add(j, new Vector<LineString>());
		}
		
		for (int i = 0; i < boundarys.size(); i++){
			LineString line = boundarys.elementAt(i);
			double ymin_vector = Math.max (line.getPointN(0).getY(), line.getPointN(1).getY());
			double ymax_vector = Math.min (line.getPointN(0).getY(), line.getPointN(1).getY());
			int ymax_raster = Rounding.getcelling(RVConversion.vectortoraster_y(ymax_vector));
			int ymin_raster = Rounding.getfloor(RVConversion.vectortoraster_y(ymin_vector));
			
//			System.out.println(y_down+" "+ ymin_raster + " " + ymax_raster);
			for (int j = ymin_raster; j <= ymax_raster; j++){	
				s.get(j).add(boundarys.elementAt(i));
			}
		}
		
		return s;
		
	}
	
	private static Vector<Coordinate> get_inside_points(int lineindex, double Minx, double Maxx, Vector<LineString> boundary){
		Vector<LineString> intersected_line=  new Vector<LineString>();
		Vector<Coordinate> cross_points=  new Vector<Coordinate>();
		double y_in_vector = RVConversion.rastertovector_y(lineindex);
			
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		
		LineString line = geometryFactory.createLineString(new Coordinate[] {new Coordinate(Minx, y_in_vector), new Coordinate(Maxx, y_in_vector)});
		for (int i = 0; i < boundary.size(); i++){
			if (boundary.elementAt(i).intersects(line)){
				intersected_line.add(boundary.elementAt(i));
			}
		}
		for (int i = 0; i < intersected_line.size(); i++){
			Geometry intersection = intersected_line.elementAt(i).intersection(line);
			Coordinate[] coords = intersection.getCoordinates();
			if (coords.length == 1){
				Coordinate point_in_raster= new Coordinate(RVConversion.vectortoraster_x(coords[0].x), RVConversion.vectortoraster_y(coords[0].y));
//				System.out.println(point_in_raster);
				cross_points.add(point_in_raster);
				
			} else {
				System.out.println(coords.length + " intersection between two lines");
			}
			
		}
		
		int x_left = Rounding.getfloor(RVConversion.vectortoraster_x(Minx));
		int x_right = Rounding.getcelling(RVConversion.vectortoraster_x(Maxx));
//		System.out.println(x_left + " " + x_right);
		Vector<Coordinate> points_inside = new Vector<Coordinate>();
		double [] all_x = new double [cross_points.size()];
//		double [] all_x_even = new double [cross_points.size()];
//		double [] all_x_odd = new double [cross_points.size()];
		for (int i = 0; i < all_x.length; i++){
			all_x[i] = cross_points.elementAt(i).x;
//			System.out.print(all_x[i]+ " ");
		}
		
		Arrays.sort(all_x);
		
//		System.out.println(" ");
		for (int i = 0; i < all_x.length; i = i + 2){
			for (int j = Rounding.getcelling(all_x[i]); j <=  Rounding.getfloor(all_x[i+1]); j++){
				points_inside.add((new Coordinate(j,lineindex)));
			}
		}

		return points_inside;	
	}
}
