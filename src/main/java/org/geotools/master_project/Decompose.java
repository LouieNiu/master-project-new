package org.geotools.master_project;

import java.io.IOException;
import java.util.Vector;

import org.geotools.filter.text.cql2.CQLException;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;

//Decompose multipolygon to polygon array
public class Decompose {
	 static Vector<Geometry> get_decompose_array(String countryname) throws IOException, CQLException {
		MultiPolygon multipolyon = (MultiPolygon) Getgeom.getgeom(countryname);
		Vector<Geometry> allpolygons = new Vector<Geometry>();
		for (int i = 0; i < multipolyon.getNumGeometries(); i++) {
			Polygon polygon = (Polygon) multipolyon.getGeometryN(i);
			allpolygons.add(polygon);
		}
		return allpolygons;
	}
}
