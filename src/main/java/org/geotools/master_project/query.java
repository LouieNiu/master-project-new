package org.geotools.master_project;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import javax.imageio.ImageIO;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.io.AbstractGridFormat;
import org.geotools.coverage.grid.io.GridCoverage2DReader;
import org.geotools.coverage.grid.io.GridFormatFinder;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

public class query {


	
	public static void main(String[] args) throws CQLException, IOException {
		// Read size of raster data
		GetRasterSize.getrastersize("glc2000_v1_1.tif");
		System.out.println("The size of raster data is " + GetRasterSize.rastersize[0] + " " + GetRasterSize.rastersize[1]);
		List<String> countries = Arrays.asList("Singapore", "Norway", "Germany", "France", "Chile","China", "Canada", "Russia");
		for (int i = 0; i < countries.size(); i++) {
			 if (i != 100){
//				 IntersectionMethod.intersect_method(countries.get(i));
//				 Naive_Method.naiveMBR(countries.get(i));
//				 Decomposing_Method.decompose( countries.get(i));
				 QuadraticSplit_Method.cross_split(countries.get(i)); 

			 }
		}
//		readimage ("glc2000_v1_1.tif");
//		Test.test_split();
	}
	

/*	
	long startTime1 = System.currentTimeMillis();

	long endTime1 = System.currentTimeMillis();
	System.out.println("Total number of polygons of "+ countryname+ " after decomposing is "+allpolygons.size());
	
	long startTime2 = System.currentTimeMillis();

	long endTime2 = System.currentTimeMillis();
	System.out.println("Total number of Pixels is "+points.size());
	
	long startTime3 = System.currentTimeMillis();
	double result = AggregateOfPoints.aggregate(raster, points);
	long endTime3 = System.currentTimeMillis();
	
	long duration1 =  endTime1 - startTime1;
	long duration2 =  endTime2 - startTime2;
	long duration3 =  endTime3 - startTime3;
	
	
	System.out.println("GetPolygon time of Decompose method is " + duration1 + " ms");
	System.out.println("Get Pixels time of Decompose method is " + duration2 + " ms");
	System.out.println("Aggregate  time of Decompose method is " + duration3 + " ms");
*/	
	



	
	// binary_split function. Get polygon array after splitting. Then do calculation.
	
	



	//Get polygon array. First decompose multipolygon, then recursively split each polygon until threshold.


	//Recursively call split method. Threshold definition is here.

	//Calculation method



	
	//Read information in each pixel

	

	



	
	// Get MBR





	//Get average of double array




	


}