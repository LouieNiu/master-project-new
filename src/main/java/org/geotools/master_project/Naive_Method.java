package org.geotools.master_project;

import java.awt.image.Raster;
import java.io.IOException;
import java.util.Vector;

import org.geotools.filter.text.cql2.CQLException;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

//Main part of Naive Method
public class Naive_Method {
	public static void naiveMBR(String countryname) throws IOException, CQLException {
		Raster raster = Readimage.readimage("glc2000_v1_1.tif");
		
		long startTime1 = System.currentTimeMillis();
		Vector <Geometry> geom = new Vector <Geometry>();
		geom.add(Getgeom.getgeom(countryname));
		long endTime1 = System.currentTimeMillis();
		
		long startTime2 = System.currentTimeMillis();
		Vector<Coordinate> points = CalculationPolygonArray.calculation_of_polygon_array(raster, geom);
		long endTime2 = System.currentTimeMillis();
		System.out.println("Total number of Pixels is "+points.size());
		
		long startTime3 = System.currentTimeMillis();
		double result = AggregateOfPoints.aggregate(raster, points);
		long endTime3 = System.currentTimeMillis();
		
		long duration1 =  endTime1 - startTime1;
		long duration2 =  endTime2 - startTime2;
		long duration3 =  endTime3 - startTime3;
		
		System.out.println("GetPolygon time of Naive method is " + duration1 + " ms");
		System.out.println("Get Pixels time of Naive method is " + duration2 + " ms");
		System.out.println("Aggregate  time of Naive method is " + duration3 + " ms");
		

	}
}
