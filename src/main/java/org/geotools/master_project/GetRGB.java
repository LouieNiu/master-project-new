package org.geotools.master_project;

import java.awt.image.Raster;
import java.io.IOException;
import java.util.Vector;

import com.vividsolutions.jts.geom.Coordinate;

//Read RGB value
public class GetRGB {
	public static int[] get_rgb(Raster raster, Vector<Coordinate> points) throws IOException {
		int [] rgb = new int [points.size()];
		for (int i = 0; i < points.size(); i++) {
			raster.getPixel(points.elementAt(i).X, points.elementAt(i).Y, rgb);
		}
		return rgb;
	}
}
