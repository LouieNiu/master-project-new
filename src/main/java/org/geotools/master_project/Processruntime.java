package org.geotools.master_project;

import java.io.IOException;

import org.geotools.filter.text.cql2.CQLException;



public class Processruntime extends Timer {
	
	public void testMethod() throws CQLException, IOException {
		GetRasterSize.getrastersize("glc2000_v1_1.tif");
		System.out.println("The size of raster data is " + GetRasterSize.rastersize[0] + " " + GetRasterSize.rastersize[1]);
		IntersectionMethod.intersect_method("Ireland");
//		query.naiveMBR("Ireland");
		Timer.end();
	}
}
