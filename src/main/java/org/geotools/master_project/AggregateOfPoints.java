package org.geotools.master_project;

import java.awt.image.Raster;
import java.io.IOException;
import java.util.Vector;

import com.vividsolutions.jts.geom.Coordinate;

// Get RGB Value and Aggregate
public class AggregateOfPoints {

	public static double aggregate(Raster raster, Vector<Coordinate> points) throws IOException {
		Vector<Double> valuearray = new Vector<Double>();
		int rgb[] = GetRGB.get_rgb(raster, points);
		Vector<Integer> v = new Vector<Integer>();
		for (int i = 0; i < rgb.length; i++) {
			v.add(rgb[i]);
		}
		valuearray.add(GetAverage.get_average_int(v));

		return GetAverage.get_average_double(valuearray);
	}
}
