package org.geotools.master_project;

import java.io.IOException;
import java.util.Vector;

import org.geotools.filter.text.cql2.CQLException;
import org.geotools.geometry.jts.JTSFactoryFinder;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Polygon;

//A mass to test
public class Test {
	public static Vector<Geometry> test_split() throws IOException, CQLException{
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		GeometryFactory geomFactory = new GeometryFactory();
		Polygon q0 = geomFactory.createPolygon(
				geomFactory.createLinearRing(
						new Coordinate[] { new Coordinate(10, 10), new Coordinate(20, 10), new Coordinate(20, 20), new Coordinate(10, 20), new Coordinate(10, 10) }),
				null);
		Coordinate[] array_of_coords = q0.getCoordinates();
		for (int i = 0; i < array_of_coords.length; i++){
			System.out.println (array_of_coords[i]);
		}
		for (int i = 0; i < IntersectionMethod.get_boundary_segments(q0).size(); i++){
			System.out.println (IntersectionMethod.get_boundary_segments(q0).elementAt(i));
		}
		
//		Vector<Vector<LineString>> s = IntersectionMethod.get_eachline_segments(IntersectionMethod.get_boundary_segments(q0));
//		System.out.println(s);
		
		if (q0.getNumInteriorRing() == 0){
			  // polygon has not holes, so extract the exterior ring
			  // and build a multilinestring
			
			System.out.println ( q0.getExteriorRing());
			}

			else{
			  // Polygon has holes, also several boundaries. 
			  // Simply run getBoundary(), it will return a multilinestring
				System.out.println ( q0.getExteriorRing());
			}
		
		
		Vector<Geometry> aftersplit = QuadraticSplit.cross_recursive(q0);
		for (int i = 0; i < aftersplit.size(); i++){
			if (aftersplit.elementAt(i).isRectangle()){
				System.out.println("Rec");
			} else {
				System.out.println(aftersplit.elementAt(i));
			}
		}
		LineString line = geometryFactory.createLineString(new Coordinate[] {new Coordinate (10, 10), new Coordinate (10, 10)});
		System.out.println(line);
		
		
		
//		System.out.println(getenvelope(getgeom("Ireland")).getMinY());
//		System.out.println(getfloor(vectortoraster_y(getenvelope(getgeom("Ireland")).getMinY())));
//		System.out.println(rastertovector_y (getfloor(vectortoraster_y(getenvelope(getgeom("Ireland")).getMinY()))));
		
		
		return null;
	}
}
